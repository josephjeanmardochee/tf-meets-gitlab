terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "http" {
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-3"
  shared_credentials_files = ["$HOME/.aws/credentials"]
}

module "vpc" {
 source = "terraform-aws-modules/vpc/aws"

 version = "5.8.1"
 name = "vpc-from-aws-jean"
 cidr = "172.31.0.0/16"

 azs  = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
 private_subnets = ["172.31.0.0/20", "172.31.16.0/20", "172.31.32.0/20"]
}

module "ec2_instance" {
 source  = "terraform-aws-modules/ec2-instance/aws"
 version = "5.6.1"
 name = "instance-iac-advance"
 monitoring = true
 instance_type = "t2.micro"
 vpc_security_group_ids = ["sg-00134c1ce7aa24be6"]
 subnet_id     = "subnet-02185363790c92e55"
   tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}